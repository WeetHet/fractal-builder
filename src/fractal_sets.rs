use std::ops::Range;

use rayon::prelude::{IntoParallelIterator, ParallelIterator};

use crate::complex::Complex;

fn calc_cnt(mut z: Complex, c: Complex, max_iter: usize, inf: f64) -> usize {
    let mut cnt = 0;
    while cnt < max_iter && z.abs_square() <= inf {
        z = z * z / Complex::new(2.0, 0.0) + c;
        cnt += 1;
    }
    cnt
}

#[allow(unused)]
pub fn julia_set(
    real: Range<f64>,
    complex: Range<f64>,
    c: Complex,
    samples: (usize, usize),
    max_iter: usize,
) -> impl ParallelIterator<Item = (Complex, usize)> {
    let step = (
        (real.end - real.start) / samples.0 as f64,
        (complex.end - complex.start) / samples.1 as f64,
    );

    (0..(samples.0 * samples.1)).into_par_iter().map(move |p| {
        let (k, w) = (p % samples.0, p / samples.0);
        let z = Complex::new(
            real.start + step.0 * k as f64,
            complex.start + step.1 * w as f64,
        );

        (z, calc_cnt(z, c, max_iter, 1e6))
    })
}

#[allow(unused)]
pub fn mandelbrot_set(
    real: Range<f64>,
    complex: Range<f64>,
    samples: (usize, usize),
    max_iter: usize,
) -> impl ParallelIterator<Item = (Complex, usize)> {
    let step = (
        (real.end - real.start) / samples.0 as f64,
        (complex.end - complex.start) / samples.1 as f64,
    );
    (0..(samples.0 * samples.1)).into_par_iter().map(move |p| {
        let (k, w) = (p % samples.0, p / samples.0);
        let c = Complex::new(
            real.start + step.0 * k as f64,
            complex.start + step.1 * w as f64,
        );
        (c, calc_cnt(Complex::new(0.0, 0.0), c, max_iter, 1e10))
    })
}
