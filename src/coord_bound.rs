use std::ops::Range;

#[derive(Debug)]
pub struct CoordBound {
    pub rx: Range<f64>,
    pub ry: Range<f64>,
}

impl Default for CoordBound {
    fn default() -> Self {
        Self {
            rx: -4.0..2.0,
            ry: -3.0..3.0,
        }
    }
}