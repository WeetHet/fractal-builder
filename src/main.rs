mod complex;
mod coord_bound;
mod fractal_chart;
mod fractal_sets;

use fractal_chart::FractalChart;

use std::default::Default;

use iced::Sandbox;
use iced::{widget::Container, window, Element};
use iced::{Length, Settings};

fn main() {
    State::run(Settings {
        antialiasing: true,
        // default_font: Some(include_bytes!("../fonts/notosans-regular.ttf")),
        window: window::Settings {
            // size: (800, 800),
            min_size: Some((800, 800)),
            ..window::Settings::default()
        },
        ..Settings::default()
    })
    .unwrap();
}

#[derive(Debug, PartialEq)]
pub enum Message {
    _Tick,
}

struct State {
    chart: FractalChart,
}

impl Sandbox for State {
    type Message = Message;

    fn new() -> Self {
        Self {
            chart: FractalChart {},
        }
    }

    fn title(&self) -> String {
        "Fractals".to_owned()
    }

    fn update(&mut self, _message: Self::Message) {}

    fn view(&self) -> Element<'_, Self::Message> {
        Container::new(self.chart.view())
            .width(Length::Fill)
            .height(Length::Fill)
            .into()
    }
}

mod style {
    use iced::Color;

    pub struct ChartContainer;

    impl iced::widget::container::StyleSheet for ChartContainer {
        type Style = ();

        fn appearance(&self, _style: &Self::Style) -> iced::widget::container::Appearance {
            iced::widget::container::Appearance {
                background: Some(Color::BLACK.into()),
                text_color: Some(Color::WHITE),
                ..Default::default()
            }
        }
    }
}
