# Fractal Builder
## Draw and Explore Algebraic Fractals

### Build & install:
Build with `cargo build --release`, install the additional dependencies if needed

On Linux you would probably need these libraries:

- fontconfig
- pkg-config
- libX11
- libXcursor
- libXrandr
- libXi
- cmake

On macOS, you probably need:

- fontconfig
- pkg-config
- cmake

If any additional libraries are needed feel free to report this via an issue

Install with `cargo install`

Run with `cargo run --release`

**Don't try to run this without `--release` it will be REALLY slow**

### Changing fractals
Currently, the only way to choose the fractal to output is to 
change this part of code in `fractal_chart`:
```rust
let set = mandelbrot_set(
    xr.clone(),
    yr.clone(),
    (pixel_width as usize, pixel_height as usize),
    MAX_ITERATIONS,
)
.collect::<Vec<_>>()
.into_iter();
```

Change `mandelbrot_set` to for example `julia_set` to see the result
