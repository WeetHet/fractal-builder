use plotters::prelude::BLACK;
use std::ops::Range;

use iced::{
    event::Status,
    mouse::Cursor,
    widget::canvas::Event,
    Element, Length, Rectangle,
};
use once_cell::sync::OnceCell;
use plotters::{
    coord::types::RangedCoordf64,
    series::LineSeries,
    style::{HSLColor, WHITE},
};
use plotters_iced::{Chart, ChartBuilder, ChartWidget, DrawingArea, DrawingBackend};
use rayon::prelude::ParallelIterator;

#[allow(unused_imports)]
use crate::fractal_sets::{mandelbrot_set, julia_set};
pub(crate) use crate::Message;
use crate::{complex::Complex, coord_bound::CoordBound};

#[derive(Default, Debug)]
pub struct FractalChartState {
    is_pressed: bool,                // Is mouse button pressed
    start_press: Option<(f64, f64)>, // Where mouse button was first pressed
    cur_cursor: Option<(f64, f64)>,
    coords: CoordBound,
    plot_coords: OnceCell<(Range<i32>, Range<i32>)>,
}

pub struct FractalChart;

impl FractalChart {
    pub fn view(&self) -> Element<Message> {
        // eprintln!("Chart reload");
        let chart = ChartWidget::new(self)
            .width(Length::Fill)
            .height(Length::Fill);

        chart.into()
    }
}

fn get_width_height(pixel_range: &(Range<i32>, Range<i32>)) -> (i32, i32) {
    (
        pixel_range.0.end - pixel_range.0.start,
        pixel_range.1.end - pixel_range.1.start,
    )
}

/// Convert point in pixel coordinates to a point in double coordinates
fn pixel_to_point(
    p: (f64, f64),
    pixel_range: &(Range<i32>, Range<i32>),
    xr: &Range<f64>,
    yr: &Range<f64>,
) -> (f64, f64) {
    let (pixel_width, pixel_height) = get_width_height(pixel_range);

    let (x_length, y_length) = (xr.end - xr.start, yr.end - yr.start);

    let (pixel_x, pixel_y) = p;
    (
        (pixel_x - pixel_range.0.start as f64) / pixel_width as f64 * x_length + xr.start,
        yr.start
            + y_length * (pixel_height as f64 - (pixel_y - pixel_range.1.start as f64))
                / pixel_height as f64,
    )
}

impl FractalChart {
    fn draw_area<const MAX_ITERATIONS: usize, DB: DrawingBackend>(
        plot_area: &DrawingArea<DB, plotters::prelude::Cartesian2d<RangedCoordf64, RangedCoordf64>>,
        xr: &Range<f64>,
        yr: &Range<f64>,
        pixel_range: &(Range<i32>, Range<i32>),
    ) {
        let (pixel_width, pixel_height) = get_width_height(pixel_range);

        const MAX_ITERATIONS: usize = 100;
        let set = mandelbrot_set(
            xr.clone(),
            yr.clone(),
            (pixel_width as usize, pixel_height as usize),
            MAX_ITERATIONS,
        )
        .collect::<Vec<_>>()
        .into_iter();
        for (
            Complex {
                real: x,
                imaginary: y,
            },
            c,
        ) in set
        {
            if c != MAX_ITERATIONS {
                plot_area.draw_pixel((x, y), &HSLColor(c as f64 / 100.0, 1.0, 0.5))
            } else {
                plot_area.draw_pixel((x, y), &BLACK)
            }
            .expect("Failed to draw pixel");
        }
    }

    fn build_pressed<DB: DrawingBackend>(
        state: &FractalChartState,
        pixel_range: &(Range<i32>, Range<i32>),
        x_range: Range<f64>,
        y_range: Range<f64>,
        mut chart: plotters::prelude::ChartContext<
            DB,
            plotters::prelude::Cartesian2d<RangedCoordf64, RangedCoordf64>,
        >,
    ) {
        let make_rectangle = |p1: (f64, f64), p2: (f64, f64)| -> [(f64, f64); 5] {
            let (x1, y1) = p1;
            let (x2, y2) = p2;

            let (top_left_x, top_left_y) = (x1.min(x2), y1.max(y2));
            let (bottom_right_x, bottom_right_y) = (x1.max(x2), y1.min(y2));

            [
                (top_left_x, top_left_y),
                (bottom_right_x, top_left_y),
                (bottom_right_x, bottom_right_y),
                (top_left_x, bottom_right_y),
                (top_left_x, top_left_y),
            ]
        };

        if let (Some(start), Some(current)) = (state.start_press, state.cur_cursor) {
            let pts = make_rectangle(
                pixel_to_point(start, pixel_range, &x_range, &y_range),
                pixel_to_point(current, pixel_range, &x_range, &y_range),
            );
            chart
                .draw_series(LineSeries::new(pts, &WHITE))
                .expect("Failed to draw rectangle");
        }
    }

    fn process_mouse_release(state: &mut FractalChartState) {
        state.is_pressed = false;

        let coords = state.plot_coords.get().unwrap();

        let (top_left, bottom_right) = {
            let (start_x, start_y) = state.start_press.take().unwrap();
            let (cursor_x, cursor_y) = state.cur_cursor.unwrap();

            let (left_x, right_x) = if start_x < cursor_x {
                (start_x, cursor_x)
            } else {
                (cursor_x, start_x)
            };

            let (left_y, right_y) = if start_y > cursor_y {
                (start_y, cursor_y)
            } else {
                (cursor_y, start_y)
            };

            ((left_x, left_y), (right_x, right_y))
        };

        let (x1, y1) = pixel_to_point(top_left, coords, &state.coords.rx, &state.coords.ry);
        let (x2, y2) = pixel_to_point(bottom_right, coords, &state.coords.rx, &state.coords.ry);

        state.coords = CoordBound {
            rx: x1..x2,
            ry: y1..y2,
        };
    }
}

impl Chart<Message> for FractalChart {
    type State = FractalChartState;

    fn build_chart<DB: DrawingBackend>(&self, state: &Self::State, mut chart: ChartBuilder<DB>) {
        let mut chart = chart
            .margin(20)
            .x_label_area_size(10)
            .y_label_area_size(10)
            .build_cartesian_2d(state.coords.rx.clone(), state.coords.ry.clone())
            .unwrap();

        chart
            .configure_mesh()
            .disable_x_mesh()
            .disable_y_mesh()
            .draw()
            .unwrap();

        let plot_area = chart.plotting_area();
        let (x_range, y_range) = (chart.x_range(), chart.y_range());
        let pixel_range = state
            .plot_coords
            .get_or_init(|| plot_area.get_pixel_range());

        Self::draw_area::<100, DB>(plot_area, &x_range, &y_range, pixel_range);

        if state.is_pressed {
            Self::build_pressed(state, pixel_range, x_range, y_range, chart);
        }
    }

    fn update(
        &self,
        state: &mut Self::State,
        event: Event,
        _bounds: Rectangle,
        cursor: Cursor,
    ) -> (Status, Option<Message>) {
        match event {
            Event::Mouse(iced::mouse::Event::ButtonPressed(_)) => {
                // Button is pressed, start drawing a rectangle
                state.is_pressed = true;
                state.start_press =
                    Some(cursor.position().map(|p| (p.x as f64, p.y as f64)).unwrap());
            }
            Event::Mouse(iced::mouse::Event::ButtonReleased(_)) => {
                // Button is released, stop drawing a rectangle and repaint fractal
                Self::process_mouse_release(state);
            }
            Event::Mouse(iced::mouse::Event::CursorMoved { position }) => {
                if state.is_pressed {
                    state.cur_cursor = Some((position.x as f64, position.y as f64));
                }
            }
            _ => {}
        }

        (Status::Captured, None)
    }
}
